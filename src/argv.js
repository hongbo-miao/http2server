import yargs from 'yargs'

export function parse (input) {
  const argv = yargs
    .usage('Usage: $0 <path> [options]')

    .command('path', 'Directory to serve')

    .alias('p', 'port')
    .default('port', process.env.PORT || 8443, '$PORT or 8443')
    .describe('port', 'Port to listen on')

    .alias('C', 'cert')
    .default('cert', 'cert.pem')
    .describe('cert', 'Path to ssl cert file')

    .alias('K', 'key')
    .default('key', 'key.pem')
    .describe('key', 'Path to ssl key file')

    .alias('f', 'fallback')
    .default('fallback', './index.html')
    .describe('fallback', 'HTML fallback for single page apps')

    .alias('i', 'include')
    .default('include', '**/*')
    .describe('include', 'Files to push alongside the fallback')

    .alias('e', 'exclude')
    .default('exclude', '**/*.map')
    .describe('exclude', 'Files not to push')

    .array('immutable')
    .default('immutable', ['hex', 'emoji'])
    .describe('immutable', 'Revved files that never change')

    .alias('s', 'silent')
    .boolean('silent')
    .default('silent', false)
    .describe('silent', 'Suppress log messages from output')

    .boolean('cors')
    .default('cors', false)
    .describe('cors', 'Set `Access-Control-Allow-Origin`')

    .default('worker', '**/service-worker/**/*')
    .describe('worker', 'Service Worker files to never push')

    .default('scope', '/')
    .describe('scope', 'Allowed scopes for Service Workers')

    .boolean('o')
    .default('o', false)
    .describe('o', 'Open browser window after starting the server')

    .alias('h', 'help')
    .help('help')

    .alias('v', 'version')
    .version()

    .parse(input)

  return argv
}
