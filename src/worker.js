import {server} from './server'

process.once('message', ({argv}) => {
  if (argv) {
    const app = server(argv)
    app.listen(argv.port)
  }
})
