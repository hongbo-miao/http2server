import cluster from 'cluster'
import physicalCpuCount from 'physical-cpu-count'
import eventToPromise from 'event-to-promise'
import {watch} from 'chokidar'
import debounce from 'debounce-collect'

export async function master (argv) {
  const workers = []
  cluster.setupMaster({exec: require.resolve('./worker.js')})
  while (workers.length < physicalCpuCount) workers.push(cluster.fork())
  await Promise.all(workers.map((worker) => eventToPromise(worker, 'online')))
  workers.forEach((worker) => worker.send({argv}))
  await Promise.all(workers.map((worker) => eventToPromise(worker, 'listening')))

  const broadcastExpireToWorkers = debounce(function (paths) {
    for (const path of new Set(paths)) {
      for (const worker of workers) {
        worker.send({expire: path})
      }
    }
  }, 50)

  watch(argv.root, {})
    .on('change', broadcastExpireToWorkers)
    .on('unlink', broadcastExpireToWorkers)
}
