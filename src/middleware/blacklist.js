import {NotFound} from 'http-errors'

export function blacklist (paths = []) {
  const excluded = paths.map(encodeURI)
  return (request, response, next) => {
    if (excluded.includes(request.url)) {
      next(new NotFound())
    } else next()
  }
}
