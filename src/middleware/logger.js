import debug from 'debug'

export function logger () {
  const log = debug('http2server')
  return (request, response, next) => {
    log(`${request.method} ${decodeURI(request.url)}`)
    next()
  }
}
