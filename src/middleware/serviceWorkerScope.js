export function serviceWorkerScope (scope) {
  return (request, response, next) => {
    if (scope && request.headers['service-worker'] === 'script') {
      response.setHeader('service-worker-allowed', scope)
    }
    next()
  }
}
