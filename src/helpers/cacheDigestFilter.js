import {queryDigestValue} from 'cache-digest'
import {getCacheDigest} from './getCacheDigest'

export function cacheDigestFilter (request, cookies, baseUrl) {
  const cacheDigest = getCacheDigest(request, cookies)
  if (!cacheDigest) return () => true

  const digestValue = Buffer.from(cacheDigest, 'base64')

  return ({pathname}) => {
    const url = baseUrl + pathname
    const etag = undefined
    const validators = false

    try {
      queryDigestValue(
        digestValue,
        url,
        etag,
        validators
      )
      return false
    } catch (error) {
      return true
    }
  }
}
