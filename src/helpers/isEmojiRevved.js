import emojiRegex from 'emoji-regex'

export const isEmojiRevved = new RegExp(
  '.+' + // Base filename
  '[-_.]' + // Separator
  `(?:${emojiRegex().source})+` + // Emoji ranges
  '(?:\\.\\w+)+$' // File extension(s)
)
