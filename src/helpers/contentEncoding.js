import compressible from 'compressible'
import Negotiator from 'negotiator'

export function contentEncoding (request, type, compressors) {
  if (!compressible(type)) return 'identity'

  const supported = new Negotiator(request)
    .encodings(compressors)

  for (const compressor of compressors) {
    if (supported.includes(compressor)) {
      return compressor
    }
  }

  return 'identity'
}
