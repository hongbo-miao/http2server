import {freemem} from 'os'
import {delimiter} from 'path'
import LRU from 'lru-cache'

function getKey (file, encoding) {
  return encoding + delimiter + file
}

export class EncodedFileCache {
  constructor () {
    this.lru = LRU({
      max: Math.min(100e6, freemem() / 2),
      length: ({length}) => length
    })

    process.on('message', ({expire: path}) => {
      if (path) {
        this.expire(path)
      }
    })
  }

  expire (path) {
    for (const encoding of ['br', 'gzip', 'deflate', 'identity']) {
      const key = getKey(path, encoding)
      if (this.lru.has(key)) {
        this.lru.del(key)
      }
    }
  }

  has (file, encoding) {
    const key = getKey(file, encoding)
    return this.lru.has(key)
  }

  get (file, encoding) {
    const key = getKey(file, encoding)
    return this.lru.get(key)
  }

  set (file, encoding, buffer) {
    const key = getKey(file, encoding)
    return this.lru.set(key, buffer)
  }
}
