import {join, dirname, basename} from 'path'
import minimatch from 'minimatch'
import glob from 'glob'
import promisify from 'pify'
import slash from 'slash'

function getBlankCache () {
  return {
    cache: Object.create(null),
    statCache: Object.create(null),
    symlinks: Object.create(null),
    realpathCache: Object.create(null)
  }
}

let globCaches = getBlankCache

process.on('message', ({expire: path}) => {
  if (path && dependencies) {
    globCaches = getBlankCache()
    dependencies = undefined
  }
})

const indexes = ['index.html', 'index.htm']
function getPathname (root, filepath) {
  const cleaned = indexes.includes(basename(filepath))
    ? dirname(filepath) + '/'
    : filepath
  return '/' + slash(cleaned)
}

let dependencies

export async function getDependencies ({include, exclude, root, worker}) {
  if (!dependencies) {
    const options = Object.assign({
      ignore: exclude,
      cwd: root,
      // root,
      nosort: true,
      // silent: true,
      // debug: true,
      nodir: true
    }, globCaches)

    dependencies = (await promisify(glob)(include, options))
      .filter((dependency) => !minimatch(dependency, worker))
      .map((dependency) => ({
        relative: dependency,
        absolute: join(root, dependency),
        pathname: getPathname(root, dependency)
      }))
  }

  return dependencies
}
