import {PassThrough} from 'stream'
import {createDeflate} from 'zlib'
import {createGzip} from 'node-zopfli'
import {compressStream} from 'iltorb'

export function encoder (encoding) {
  if (encoding === 'br') {
    return compressStream()
  } else if (encoding === 'gzip') {
    return createGzip()
  } else if (encoding === 'deflate') {
    return createDeflate()
  } else if (encoding === 'identity') {
    return new PassThrough()
  } else {
    throw new Error(`Unsupported encoding: ${encoding}`)
  }
}
